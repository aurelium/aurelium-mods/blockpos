-- Store whether the HUD is shown for each player
local show_hud = {}

-- Register the HUD item
minetest.register_on_joinplayer(function(player)
    local player_name = player:get_player_name()
    local id = player:hud_add({
        hud_elem_type = "text",
        position = {x = 1, y = 1},
        offset = {x = -40, y = -20},
        text = "Pos: ",
        alignment = {x = -1, y = -1},
        scale = {x = 100, y = 100},
        number = 0x00000000,
        name = "mod:pos"
    })
    
    -- Store the ID in the player's meta data
    player:get_meta():set_int("mod:pos_id", id)

    -- By default, hide the HUD
    show_hud[player_name] = false
end)

-- Update the HUD item
minetest.register_globalstep(function(dtime)
    for _, player in ipairs(minetest.get_connected_players()) do
        local pos = player:get_pos()
        
        -- Convert node positions to block positions
        pos.x = math.floor(pos.x / 16)
        pos.y = math.floor(pos.y / 16)
        pos.z = math.floor(pos.z / 16)
        
        -- Get the ID from the player's meta data
        local id = player:get_meta():get_int("mod:pos_id")
        
        -- Update the HUD
        player:hud_change(id, "text", "Pos: "..minetest.pos_to_string(pos))
    end
end)

-- Register the 'blockpos' privilege
minetest.register_privilege("blockpos", {
    description = "Allows use of block position commands",
    give_to_singleplayer = false,
})

-- Add a chat command to toggle the HUD on and off
minetest.register_chatcommand("togglepos", {
    privs = {blockpos = true},
    func = function(player_name, param)
        -- Toggle the HUD for this player
        show_hud[player_name] = not show_hud[player_name]
        
        -- Get the ID from the player's meta data
        local id = minetest.get_player_by_name(player_name):get_meta():get_int("mod:pos_id")
        
        if show_hud[player_name] then
            -- Show the HUD
            minetest.get_player_by_name(player_name):hud_change(id, "number", 0xFFFFFF)  -- White color
        else
            -- Hide the HUD
            minetest.get_player_by_name(player_name):hud_change(id, "number", 0x00000000)  -- Transparent color
        end
        
        return true, "Position display toggled."
    end,
})

-- Add a chat command to teleport the player to a block position
minetest.register_chatcommand("blockteleport", {
    privs = {blockpos = true},
    params = "<x> <y> <z>",
    description = "Teleport to a block position",
    func = function(player_name, param)
        -- Split the parameters into x, y, and z
        local x, y, z = string.match(param, "^(-?%d+) (-?%d+) (-?%d+)$")
        
        if x and y and z then
            -- Convert block positions to node positions
            x = tonumber(x) * 16
            y = tonumber(y) * 16
            z = tonumber(z) * 16
            
            -- Teleport the player
            minetest.get_player_by_name(player_name):set_pos({x = x, y = y, z = z})
            
            return true, "Teleported to block position (" .. x .. ", " .. y .. ", " .. z .. ")."
        else
            return false, "Invalid parameters. Usage: /blockteleport <x> <y> <z>"
        end
    end,
})
